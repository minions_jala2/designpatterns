﻿namespace DecoratorPattern
{
    public class Cream : CondimentDecorator
    {

        private Beverage _beverage;

        public Cream(Beverage beverage)
        {
            _beverage = beverage;
        }


        public override double Cost()
        {
            return _beverage.Cost() + 0.25;
        }

        public override string Description
        {
            get
            {
                return _beverage.Description + ", Cream";    
            }
        }

    }
}
