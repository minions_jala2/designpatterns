﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Beverage beverage = new Espresso();

            Console.WriteLine("{0} -> ${1}", beverage.Description, beverage.Cost());

            beverage = new Cream(beverage);

            beverage = new Sugar(beverage);
            beverage = new Cream(beverage);

            Console.WriteLine("{0} -> ${1}", beverage.Description, beverage.Cost());
        }
    }
}
