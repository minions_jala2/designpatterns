﻿using System;

namespace DecoratorPattern
{
    public class Espresso : Beverage
    {
        public Espresso()
        {
            Description = "Cafe Expesso";
        }

        public override double Cost()
        {
            return 10.5;
        }
    }
}
