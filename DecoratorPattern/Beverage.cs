﻿namespace DecoratorPattern
{
    public abstract class Beverage
    {

        public abstract double Cost();
        
        public virtual string Description { get; set; }

    }
}
