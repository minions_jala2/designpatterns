﻿namespace DecoratorPattern
{
    public class Sugar : CondimentDecorator
    {
        private Beverage _beverage;

        public Sugar(Beverage beverage)
        {
            _beverage = beverage;
        }


        public override double Cost()
        {
            return _beverage.Cost() + 0.05;
            
        }

        public override string Description
        {
            get
            {
                return _beverage.Description + ", Sugar";
            }

        }

    }
}
